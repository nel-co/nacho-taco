import React from 'react'
import Background from './background.png';

const Hero = () => (
  <div className="hero" style={{background: `url(${Background})`}}>
    <div className="container">
      <h1>Authentic Mexican Food</h1>
      <p>Tacos, Bar, & specialty drinks</p>
    </div>
  </div>
)

export default Hero
