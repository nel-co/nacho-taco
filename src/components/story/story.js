import React from 'react'

import StoryImg from './about-us.png'

const OurStory = ( {data} ) => (
  <div className="story" id="our-story">
    <div className="container">
      <div className="story-text">
        <h3>Our Story</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus urna at sem feugiat, sed hendrerit magna viverra. Sed ut vulputate ante. Donec id augue orci. Morbi eu rutrum nulla, id maximus dolor. In vitae suscipit mi. Aenean nisi eros, placerat quis orci ac, finibus euismod nisi. Nam vestibulum arcu condimentum ante lobortis semper. In pretium mattis massa at dictum. Sed ut placerat massa. Nullam fringilla placerat feugiat.</p>
        <span>Like us on <a href="#" target="_blank">Facebook</a></span>
        <span>Follow us on <a href="#" target="_blank">Instagram</a></span>
      </div>
      <div className="story-img">
        <img src={StoryImg} alt="image from inside Nacho Taco" />
      </div>
    </div>
  </div>
)

export default OurStory
