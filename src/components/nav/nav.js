import React from 'react'
import Logo from './logo.png'

const Nav = () => (
  <div className="nav">
    <div className="container">
      <div className="nav-items">
        <a href='#'>Home</a>
        <a href='#'>Menu</a>
      </div>
      <img src={Logo} alt="Nacho Taco Logo" />
      <div className="nav-items">
        <a href='#'>Our Story</a>
        <a href='#'>Location</a>
      </div>
    </div>
  </div>
)

export default Nav
