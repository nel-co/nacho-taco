import React from 'react'

const Footer = ( {data} ) => (
  <div className="footer" id="location">
    <div className="container">
      <div className="footer-text">
        <span>open mon-sat. 11:00am-10:00pm</span>
        <span>129 N Spring Street <br /> Spartanburg, South Carolina</span>
        <span>¡nos vemos pronto!</span>
      </div>
    </div>
  </div>
)

export default Footer
