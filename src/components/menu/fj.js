import React from 'react'

const Fajitas = ( {data} ) => (
  <div className="menu-block">
    <div className="menu-info">
      <h3>Fajitas</h3>
      <p>Comes with: Tortillas, rice, beans, lettuce, sour cream, pico, peppers, onions, squash, tomatoes, zucchini, & mushrooms.</p>
    </div>
    <div className="menu-items">
      {data.allMarkdownRemark.edges.map(item => {
        if (item.node.frontmatter.type === 'fajita') {
          return (
            <div className="item" key={item.node.frontmatter.name}>
              <div className="item-info">
                <span>{item.node.frontmatter.name}</span>
                <span>{item.node.frontmatter.description !== '' ? `(${item.node.frontmatter.description})` : null}</span>
              </div>
              <div className="item-price">
                <span>${item.node.frontmatter.price}</span>
              </div>
            </div>
          );
        }
      })}
    </div>
  </div>
)

export default Fajitas
