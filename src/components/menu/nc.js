import React from 'react'

const Nachos = ( {data} ) => (
  <div className="menu-block">
    <div className="menu-info">
      <h3>Nachos</h3>
      <p>COMES WITH: MEAT & CHEESE. <span>make it a supreme for $1.00 extra</span> (lettuce, onion, cilantro, tomatioes, jalpeno, & sour cream)</p>
    </div>
    <div className="menu-items">
      {data.allMarkdownRemark.edges.map(item => {
        if (item.node.frontmatter.type === 'nacho') {
          return (
            <div className="item" key={item.node.frontmatter.name}>
              <div className="item-info">
                <span>{item.node.frontmatter.name}</span>
                <span>{item.node.frontmatter.description !== '' ? `(${item.node.frontmatter.description})` : null}</span>
              </div>
              <div className="item-price">
                <span>${item.node.frontmatter.price}</span>
              </div>
            </div>
          );
        }
      })}
    </div>
  </div>
)

export default Nachos
