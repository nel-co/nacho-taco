import React from 'react'

const Tortas = ( {data} ) => (
  <div className="menu-block">
    <div className="menu-info">
      <h3>Tortas</h3>
      <p>Tortas are mexican sandwiches stuffed with sour cream, beans, lettuce, onions, cilantra, avocado, tomatoes, and pressed to a crisp. <span>Choose 1 meat.</span></p>
    </div>
    <div className="menu-items">
      {data.allMarkdownRemark.edges.map(item => {
        if (item.node.frontmatter.type === 'torta') {
          return (
            <div className="item" key={item.node.frontmatter.name}>
              <div className="item-info">
                <span>{item.node.frontmatter.name}</span>
                <span>{item.node.frontmatter.description !== '' ? `(${item.node.frontmatter.description})` : null}</span>
              </div>
              <div className="item-price">
                <span>${item.node.frontmatter.price}</span>
              </div>
            </div>
          );
        }
      })}
    </div>
  </div>
)

export default Tortas
