import React from 'react'

const Quesadilla = ( {data} ) => (
  <div className="menu-block">
    <div className="menu-info">
      <h3>Quesadillas</h3>
      <p>stuffed with your choice of meat and a side of lettuce garnished with tomato.</p>
    </div>
    <div className="menu-items">
      {data.allMarkdownRemark.edges.map(item => {
        if (item.node.frontmatter.type === 'quesadilla') {
          return (
            <div className="item" key={item.node.frontmatter.name}>
              <div className="item-info">
                <span>{item.node.frontmatter.name}</span>
                <span>{item.node.frontmatter.description !== '' ? `(${item.node.frontmatter.description})` : null}</span>
              </div>
              <div className="item-price">
                <span>${item.node.frontmatter.price}</span>
              </div>
            </div>
          );
        }
      })}
    </div>
  </div>
)

export default Quesadilla
