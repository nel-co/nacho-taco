import React from 'react'
import Quesadilla from './qs'
import Tostada from './ts'
import Tortas from './tr'
import Fajitas from './fj'
import Tacos from './tc'
import Nachos from './nc'

const Menu = ( {data} ) => (
  <div className="menu" id="menu">
    <div className="container" data-isotope='{ "itemSelector": ".menu-block", "percentPosition": "true", "sortBy": "original-order", "masonry": { "gutter": 15 } }'>
      <Quesadilla data={data} />
      <Tostada data={data} />
      <Tortas data={data} />
      <Fajitas data={data} />
      <Tacos data={data} />
      <Nachos data={data} />
    </div>
  </div>
)

export default Menu
