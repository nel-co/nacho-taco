import React from 'react'

const Tacos = ( {data} ) => (
  <div className="menu-block">
    <div className="menu-info">
      <h3>Tacos</h3>
      <p>CHoose your tortilla. <span>Either Corn or Flour.</span> choose your meat. <span>Just 1 type.</span> pick 2 toppings. <span>we have lettuce, cheese, tomatoes, onions, cilantro, & avocado.</span> Extra toppings are just $.15</p>
    </div>
    <div className="menu-items">
      {data.allMarkdownRemark.edges.map(item => {
        if (item.node.frontmatter.type === 'taco') {
          return (
            <div className="item" key={item.node.frontmatter.name}>
              <div className="item-info">
                <span>{item.node.frontmatter.name}</span>
                <span>{item.node.frontmatter.description !== '' ? `(${item.node.frontmatter.description})` : null}</span>
              </div>
              <div className="item-price">
                <span>${item.node.frontmatter.price}</span>
              </div>
            </div>
          );
        }
      })}
    </div>
  </div>
)

export default Tacos
