import React from 'react'

const Specials = ( {data} ) => (
  <div className="specials">
    <div className="container">
      {data.allMarkdownRemark.edges.map(item => {
        if(item.node.frontmatter.special !== null) {
          return <h3 key={item.node.frontmatter.special}>{item.node.frontmatter.special}</h3>
        }
      })}
    </div>
  </div>
)

export default Specials
