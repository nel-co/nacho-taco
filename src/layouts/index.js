import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Nav from '../components/nav/nav'
import '../style/main.scss'

const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title='Nacho Taco - Spartanburg, South Carolina'
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
      link={[
        {rel: 'preload', as: 'style', href: 'https://fonts.googleapis.com/css?family=Dokdo'},
      ]}
      script={[
        {src: 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js'}
      ]}
    />
    <Nav />
    <div>
      {children()}
    </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout


