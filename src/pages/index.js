import React from 'react'
import Hero from '../components/hero/hero'
import Specials from '../components/specials/specials'
import Menu from '../components/menu/menu'
import OurStory from '../components/story/story'
import Footer from '../components/footer/footer'

const IndexPage = ({data}) => (
  <div>
    {console.log(data)}
    <Hero />
    <Specials data={data} />
    <Menu data={data} />
    <OurStory />
    <Footer />
  </div>
)

export default IndexPage


export const query = graphql`
  query MenuItems {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            type
            name
            price
            description
            special
          }
        }
      }
    }
  }
`;